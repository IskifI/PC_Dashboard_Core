﻿// Объект тест рана
namespace PC_Dashboard_Core.Models
{
    public class TestRunModel
    {
        public string RunId { get; set; }
        public string PassedTest { get; set; }
        public string ResultUrl { get; set; }
        public string OfflineResult { get; set; }
    }
}
