﻿using System.Collections.Generic;
using System.Linq;

// Объект проекта
namespace PC_Dashboard_Core.Models
{
    public class ProjectModel
    {
        public string Name { get; set; }
        public List<TestRunModel> TestRun { get; set; }
        public string TestUrl { get; set; }
        public int PageNum { get; set; }
        public List<TestRunModel> TestRunView { get; set; }

        public ProjectModel()
        {
            TestRun = new List<TestRunModel>();
            TestRunView = new List<TestRunModel>();
            TestRunView = GetTestRun(TestRun, PageNum);
        }

        // Формирует список тест ранов для отображения на постраничном выводе
        public List<TestRunModel> GetTestRun(List<TestRunModel> allRuns, int pageNum)
        {
            List<TestRunModel> result = new List<TestRunModel>();
            int pageSize = 10;

            int count = allRuns.Count();
            result = allRuns.Skip((pageNum - 1) * pageSize).Take(pageSize).ToList();

            return result;
        }
    }
}
