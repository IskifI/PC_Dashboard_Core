﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.IO.Compression;

namespace PC_Dashboard_Core
{
    public class APIConnect
    {
        // Асинхронная функция логина в ALM API
        public static async Task<string> Login()
        {
            string cookie = "";
            try
            {
                string logpass = "Basic " + AlmProject.Convert64(GlobalData.login, GlobalData.password);

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(GlobalData.serverProtocol +
                    GlobalData.serverDomen + ":" + GlobalData.serverPort + "/qcbin/api/authentication/sign-in");
                request.Method = "POST";
                request.Headers["Authorization"] = logpass;

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                cookie = response.Headers["Set-Cookie"];
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[Login] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return cookie;
        }

        // Асинхронная функция авторизации в PC API
        public static async Task<string[,]> LoginPC()
        {
            string logpass = "Basic " + AlmProject.Convert64(GlobalData.login, GlobalData.password);
            string[,] cookies = new string[2, 2];

            try
            {
                HttpWebRequest requestLogin = (HttpWebRequest)WebRequest.Create(GlobalData.serverPcProtocol + GlobalData.serverPcDomen
                    + "/LoadTest/rest/authentication-point/authenticate");
                requestLogin.Method = "GET";
                requestLogin.Headers["Authorization"] = logpass;
                HttpWebResponse responseLogin = (HttpWebResponse)await requestLogin.GetResponseAsync();
                string cookie = responseLogin.Headers["Set-Cookie"];

                // Извлекает LWSSO_COOKIE_KEY
                cookies[0, 0] = cookie.Substring(0, cookie.IndexOf("="));
                cookies[0, 1] = cookie.Substring(cookie.IndexOf("=") + 1, cookie.IndexOf(";") - cookie.IndexOf("=") - 1);
                cookie = cookie.Remove(0, cookie.IndexOf(",") + 1);
                // Извлекает QCSession
                cookies[1, 0] = cookie.Substring(0, cookie.IndexOf("="));
                cookies[1, 1] = cookie.Substring(cookie.IndexOf("=") + 1, cookie.IndexOf(";") - cookie.IndexOf("=") - 1);
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[LoginPC] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return cookies;
        }
        // Асинхронная функция получения проектов на сервере ALM.
            public static async Task<string> TakeProject()
        {
            string takenProjects = "";

            try
            {
                string[,] cook = AlmProject.ParseCookie();

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(GlobalData.serverProtocol +
                    GlobalData.serverDomen + ":" + GlobalData.serverPort + "/qcbin/rest/domains/" +
                    GlobalData.almDomenName + "/projects");
                request.Method = "GET";
                request.Headers["Accept"] = "application/xml";
                request.Headers["Cookie"] = cook[0, 0] + "=" + cook[0, 1] + "; "
                                            + cook[1, 0] + "=" + cook[1, 1] + "; "
                                            + cook[2, 0] + "=" + cook[2, 1] + "; "
                                            + cook[3, 0] + "=" + cook[3, 1];

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                StreamReader res = new StreamReader(response.GetResponseStream());
                takenProjects = res.ReadLine();
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[TakeProject] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return takenProjects;
        }

        // Асинхронная функция получения запущенных тестов на сервере ALM.
        public static async Task<string> TakeRunTest(string domenName, string projectName)
        {
            string takenRunTests = "";

            try
            {
                string[,] cook = AlmProject.ParseCookie();

                string uri = GlobalData.serverProtocol + GlobalData.serverDomen + ":" +
                    GlobalData.serverPort + "/qcbin/rest/domains/" + domenName + "/projects/" + projectName + "/runs";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                request.Method = "GET";
                request.Headers["Accept"] = "application/xml";
                request.Headers["Cookie"] = cook[0, 0] + "=" + cook[0, 1] + "; "
                                            + cook[1, 0] + "=" + cook[1, 1] + "; "
                                            + cook[2, 0] + "=" + cook[2, 1] + "; "
                                            + cook[3, 0] + "=" + cook[3, 1];

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                StreamReader res = new StreamReader(response.GetResponseStream());
                takenRunTests = res.ReadLine();
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[TakeRunTest] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return takenRunTests;
        }

        // Асинхронная функция получения ResultID на сервере PC.
        public static async Task<string> GetResultID(string projectName, string runID)
        {
            string resultIdXML = "";
            string resultID = "";
            List<string> takenRunId = new List<string>();
            string[,] cookies = LoginPC().Result;

            try
            {
                string uri = GlobalData.serverPcProtocol + GlobalData.serverPcDomen 
                    + "/LoadTest/rest/domains/" + GlobalData.almDomenName + "/projects/" + projectName + "/Runs/" 
                    + runID + "/Results";

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                
                request.Method = "GET";
                request.Headers["Cookie"] = cookies[0, 0] + "=" + cookies[0, 1] + "; "
                                            + cookies[1, 0] + "=" + cookies[1, 1] + "; ";

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                StreamReader res = new StreamReader(response.GetResponseStream());
                resultIdXML = res.ReadToEnd();

                if (resultIdXML.Contains("<RunResult>"))
                {
                    // Создаём массив из всех результатов
                    while (resultIdXML.Contains("<RunResult>"))
                    {
                        takenRunId.Add(resultIdXML.Substring(resultIdXML.IndexOf("<RunResult>"),
                            resultIdXML.IndexOf("</RunResult>") - resultIdXML.IndexOf("<RunResult>")));
                        resultIdXML = resultIdXML.Remove(0, resultIdXML.IndexOf("</RunResult>") + 12);
                    }
                    // Проверяем есть ли HTML репорт и если есть, извлекаем его ID
                    foreach (string enti in takenRunId)
                    {
                        string resStatus = enti.Substring(enti.IndexOf("<Type>") + 6, enti.IndexOf("</Type>") - enti.IndexOf("<Type>") - 6);
                        if (resStatus == "HTML REPORT")
                        {
                            resultID = enti.Substring(enti.IndexOf("<ID>") + 4, enti.IndexOf("</ID>") - enti.IndexOf("<ID>") - 4);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[GetResultID] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return resultID;
        }

        
        // Асинхронная функция получения HTML Report с сервера PC.
        public static async Task<string> GetHtmlReport(string runID, string resultId, string projectName)
        {
            string projectDir = System.AppDomain.CurrentDomain.BaseDirectory + "wwwroot\\HTML_Results\\" + projectName;
            string zipHtmlRepor_path = System.AppDomain.CurrentDomain.BaseDirectory + "wwwroot\\HTML_Results\\" + projectName + "\\Results_" + runID + ".zip";
            string htmlReport_path = "/HTML_Results/" + projectName + "/Results_" + runID + "/";
            string htmlReport_path_unzip = System.AppDomain.CurrentDomain.BaseDirectory + "wwwroot\\HTML_Results\\" + projectName + "\\Results_" + runID;
            string[,] cookies = LoginPC().Result;

            try
            {
                if (resultId != "")
                {
                    string uri = GlobalData.serverPcProtocol + GlobalData.serverPcDomen
                    + "/LoadTest/rest/domains/" + GlobalData.almDomenName + "/projects/" + projectName + "/Runs/"
                    + runID + "/Results/" + resultId + "/data";

                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
                    request.Method = "GET";
                    request.Headers["Cookie"] = cookies[0, 0] + "=" + cookies[0, 1] + "; "
                                                + cookies[1, 0] + "=" + cookies[1, 1] + "; ";

                    HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();

                    // Создание потока с проверкой, размера входного потока
                    MemoryStream ms = new MemoryStream(response.ContentLength > 0 ? (int)response.ContentLength : 2000000);
                    using (Stream res = response.GetResponseStream())
                    {
                        res.CopyTo(ms);
                    }
                    // Создание массива майтов из потока
                    byte[] bt = ms.ToArray();

                    // Создание необходимых директорий
                    Directory.CreateDirectory(projectDir);
                    if (Directory.Exists(htmlReport_path_unzip))
                    {
                        Directory.Delete(htmlReport_path_unzip, true);
                        Directory.CreateDirectory(htmlReport_path_unzip);
                    }
                    else
                    {
                        Directory.CreateDirectory(htmlReport_path_unzip);
                    }

                    // Запись массива байт в файл
                    File.WriteAllBytes(zipHtmlRepor_path, bt);
                    // Распаковка отчёта
                    ZipFile.ExtractToDirectory(zipHtmlRepor_path, htmlReport_path_unzip);
                }
                else
                {
                    htmlReport_path = "";
                }
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[GetHtmlReport] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return htmlReport_path;
        }
    }
}
