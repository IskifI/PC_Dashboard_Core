﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using PC_Dashboard_Core.Models;

namespace PC_Dashboard_Core
{
    public class AlmProject
    {
        // Получает данные и создаёт массив для отображения на странице.
        public static List<ProjectModel> ProjectList()
        {
            List<ProjectModel> projects = new List<ProjectModel>();

            foreach (string projectName in ParseXML(APIConnect.TakeProject().Result))
            {
                try
                {
                    string xmlData = APIConnect.TakeRunTest(GlobalData.almDomenName, projectName).Result;
                    List<string> runIdList = ParseTestRunXML(xmlData);
                    List<string> passedTestList = ParseTestPassDateXML(xmlData);
                    List<string> resultUriList = new List<string>();
                    foreach (string runIdInList in runIdList)
                    {
                        resultUriList.Add("td://" + projectName + "." + GlobalData.almDomenName + "." +
                            GlobalData.serverDomen + ":" + GlobalData.serverPort + "/qcbin/TestRunsModule-00000000090859589?EntityType=IRun&EntityID=" + runIdInList);
                    }
                    string runMonitorUri = ParseRunMonitorXML(xmlData);
                    List<TestRunModel> testRunList = new List<TestRunModel>();
                    for (int i = 0; i < runIdList.Count(); i++)
                    {
                        TestRunModel testRun = new TestRunModel
                        {
                            RunId = runIdList[i],
                            PassedTest = passedTestList[i],
                            ResultUrl = resultUriList[i]
                        };
                        testRunList.Add(testRun);
                    }
                    testRunList.Reverse();
                    ProjectModel project = new ProjectModel()
                    {
                        Name = projectName,
                        TestRun = testRunList,
                        TestUrl = runMonitorUri,
                        PageNum = 1,
                    };
                    project.TestRunView = project.GetTestRun(testRunList, 1);
                    projects.Add(project);
                }
                catch (Exception ex)
                {
                    string logFile_path = @"C:\log\apdb.log";
                    using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                    {
                        sw.WriteLine("[" + DateTime.Now + "] " + "[ParseXML] " + GlobalData.login + " " + ex.StackTrace);
                    }
                }
            }
            return projects;
        }

        // Кодирует логин и пароль в Base64 для логина на сервере.
        // Сервер ALM принимает логин и пароль в заголовке POST запроса только в формате Base64.
        public static string Convert64(string login, string password)
        {
            string logpass = "";
            try
            {
                string login_password = login + ":" + password;
                char[] LoginPasswordString = login_password.ToCharArray();
                byte[] binaryData = new byte[LoginPasswordString.Length];
                int i = 0;
                while (i < LoginPasswordString.Length)
                {
                    binaryData[i] = Convert.ToByte(LoginPasswordString[i]);
                    i++;
                }

                logpass = Convert.ToBase64String(binaryData);
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[Convert64] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return logpass;
        }

        // Извлекает куки из заголовка и возвращает их значение.
        public static string[,] ParseCookie()
        {
            string[,] cookies = new string[4, 2];

            try
            {
                string cookie = APIConnect.Login().Result.ToString();

                // Извлекает LWSSO_COOKIE_KEY
                cookies[0, 0] = cookie.Substring(0, cookie.IndexOf("="));
                cookies[0, 1] = cookie.Substring(cookie.IndexOf("=") + 1, cookie.IndexOf(";") - cookie.IndexOf("=") - 1);
                cookie = cookie.Remove(0, cookie.IndexOf(",") + 1);
                // Извлекает QCSession
                cookies[1, 0] = cookie.Substring(0, cookie.IndexOf("="));
                cookies[1, 1] = cookie.Substring(cookie.IndexOf("=") + 1, cookie.IndexOf(";") - cookie.IndexOf("=") - 1);
                cookie = cookie.Remove(0, cookie.IndexOf(",") + 1);
                // Извлекает ALM_USER
                cookies[2, 0] = cookie.Substring(0, cookie.IndexOf("="));
                cookies[2, 1] = cookie.Substring(cookie.IndexOf("=") + 1, cookie.IndexOf(";") - cookie.IndexOf("=") - 1);
                cookie = cookie.Remove(0, cookie.IndexOf(",") + 1);
                // Извлекает XSRF-TOKEN
                cookies[3, 0] = cookie.Substring(0, cookie.IndexOf("="));
                cookies[3, 1] = cookie.Substring(cookie.IndexOf("=") + 1, cookie.IndexOf(";") - cookie.IndexOf("=") - 1);

                GlobalData.cookies = cookies;
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "]" + " [ParseCookie] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return cookies;
        }

        // Парсер XML списка проектов.
        public static List<string> ParseXML(string xmlData)
        {

            List<string> result = new List<string>();

            foreach (Match m in Regex.Matches(xmlData, @"Name\S{2}\w+\S?/>"))
            {
                try
                {
                    string res = m.ToString();
                    result.Add(res.Substring(res.IndexOf("\"") + 1, res.IndexOf(">") - res.IndexOf("\"") - 3));
                }
                catch (Exception ex)
                {
                    string logFile_path = @"C:\log\apdb.log";
                    using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                    {
                        sw.WriteLine("[" + DateTime.Now + "]" + " [ParseXML] " + GlobalData.login + " " + ex.StackTrace);
                    }
                }
            }

            return result;
        }

        // Парсер XML списка запущенных тестов. Получаем последний запущенный тест.
        // Если тесты в проекте не запускались выдаёт 0.
        public static List<string> ParseTestRunXML(string xmlData)
        {

            List<string> takenTests = new List<string>();
            List<string> takenRunId = new List<string>();

            // Если в проекте есть запущенные тесты
            try
            {
                if (xmlData.Contains("<Entity "))
                {
                    // Создаём массив из всех тестов
                    while (xmlData.Contains("<Entity "))
                    {
                        takenTests.Add(xmlData.Substring(xmlData.IndexOf("<Entity "),
                            xmlData.IndexOf("</Entity>") - xmlData.IndexOf("<Entity ")));
                        xmlData = xmlData.Remove(0, xmlData.IndexOf("</Entity>") + 9);
                    }
                    // Проверяем успешность теста
                    foreach (string enti in takenTests)
                    {
                        string entiRem = enti.Remove(0, enti.IndexOf("\"state\""));
                        string resStatus = entiRem.Substring(15, entiRem.IndexOf("</") - 15);
                        if (resStatus == "Before Collating Results" || resStatus == "Before Creating Analysis Data"
                            || resStatus == "Finished")
                        {
                            entiRem = enti.Remove(0, enti.IndexOf("\"id\""));
                            string runID = entiRem.Substring(12, entiRem.IndexOf("</") - 12);
                            takenRunId.Add(runID);
                        }
                        //else takenRunId.Add("0");
                    }
                }
                else takenRunId.Add("Тестов не проводилось");
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "]" + " [ParseTestRunXML] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return takenRunId;
        }

        // Парсер XML списка запущенных тестов. Получаем дату и время последнего успешного теста.
        public static List<string> ParseTestPassDateXML(string xmlData)
        {
            List<string> takenTests = new List<string>();
            List<string> takenDateTime = new List<string>();

            // Если в проекте есть запущенные тесты
            try
            {
                if (xmlData.Contains("<Entity "))
                {
                    // Создаём массив из всех тестов
                    while (xmlData.Contains("<Entity "))
                    {
                        takenTests.Add(xmlData.Substring(xmlData.IndexOf("<Entity "),
                            xmlData.IndexOf("</Entity>") - xmlData.IndexOf("<Entity ")));
                        xmlData = xmlData.Remove(0, xmlData.IndexOf("</Entity>") + 9);
                    }
                    // Проверяем успешность теста и извлекаем из него дату и время
                    foreach (string enti in takenTests)
                    {
                        string entiRem = enti.Remove(0, enti.IndexOf("\"state\""));
                        string resStatus = entiRem.Substring(15, entiRem.IndexOf("</") - 15);
                        if (resStatus == "Before Collating Results" || resStatus == "Before Creating Analysis Data"
                            || resStatus == "Finished")
                        {
                            entiRem = enti.Remove(0, enti.IndexOf("\"execution-date\""));
                            string date = entiRem.Substring(24, entiRem.IndexOf("</") - 24);
                            entiRem = enti.Remove(0, enti.IndexOf("\"execution-time\""));
                            string time = entiRem.Substring(24, entiRem.IndexOf("</") - 24);
                            takenDateTime.Add(date + " / " + time);
                        }
                        //else takenDateTime.Add("Нет успешных тестов");
                    }
                }

                else takenDateTime.Add("Тестов не проводилось");
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "]" + " [ParseTestPassDateXML] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return takenDateTime;
        }

        // Парсер XML списка запущенных тестов. Получаем uri на запущенный тест.
        public static string ParseRunMonitorXML(string xmlData)
        {
            List<string> takenTests = new List<string>();
            string uri = "#";

            // Если в проекте есть запущенные тесты
            try
            {
                if (xmlData.Contains("<Entity "))
                {
                    // Создаём массив из всех тестов
                    while (xmlData.Contains("<Entity "))
                    {
                        takenTests.Add(xmlData.Substring(xmlData.IndexOf("<Entity "),
                            xmlData.IndexOf("</Entity>") - xmlData.IndexOf("<Entity ")));
                        xmlData = xmlData.Remove(0, xmlData.IndexOf("</Entity>") + 9);
                    }

                    // Проверяем запущен тест или нет, если запущен извлекаем ссылку на мониторинг в PC
                    string strRem = takenTests.Last<string>().Remove(0, takenTests.Last<string>().IndexOf("\"state\""));
                    string resStatus = strRem.Substring(15, strRem.IndexOf("</") - 15);
                    if (resStatus == "Running")
                    {
                        uri = takenTests.Last<string>().Remove(0, takenTests.Last<string>().IndexOf("\"pc-run-url\""));
                        uri = uri.Substring(20, uri.IndexOf("</") - 20);
                        uri = GlobalData.serverPcProtocol + GlobalData.serverPcDomen + "/loadtest/" + uri;
                    }

                    //else uri = "#";
                }

                //else uri = "#";
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "]" + " [ParseRunMonitorXML] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return uri;
        }
    }
}
