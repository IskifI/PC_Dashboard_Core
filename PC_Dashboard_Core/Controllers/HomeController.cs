﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using PC_Dashboard_Core.Models;

namespace PC_Dashboard_Core.Controllers
{
    public class HomeController : Controller
    {
        // Стартовая страница сайта
        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        // Обработка данных после ввода логопаса и редирект на страницу с проектами
        [HttpPost]
        public IActionResult Index(string login, string password)
        {
            HttpContext.Session.SetString("login", login);
            HttpContext.Session.SetString("password", password);

            GlobalData.login = HttpContext.Session.GetString("login");
            GlobalData.password = HttpContext.Session.GetString("password");

            HttpContext.Session.Set<List<ProjectModel>>("projects", AlmProject.ProjectList());
            GlobalData.projects = HttpContext.Session.Get<List<ProjectModel>>("projects");
            HttpContext.Session.Set<string[,]>("cookies", GlobalData.cookies);

            // Если по указанной УЗ нет проектов, говорим об этом пользователю
            if (GlobalData.projects.Count != 0)
            {
                return RedirectToAction("About");
            }
            else
            {
                return View("NoAuth");
            }
        }

        // Вывод страницы тест ранов по проектам, после входа на сайт
        [HttpGet]
        public IActionResult About()
        {
            return View(GlobalData.projects);
        }

        // Вывод страницы тест ранов по проектам, при использовании пагинации
        [HttpPost]
        public IActionResult About(string project, int pageNum)
        {
            List<ProjectModel> projects = HttpContext.Session.Get<List<ProjectModel>>("projects");
            for (int i = 0; i < projects.Count(); i++)
            {
                if (projects[i].Name == project)
                {
                    projects[i].PageNum = pageNum;
                    projects[i].TestRunView = projects[i].GetTestRun(projects[i].TestRun, pageNum);
                }
                else
                {
                    projects[i].PageNum = 1;
                    projects[i].TestRunView = projects[i].GetTestRun(projects[i].TestRun, 1);
                }
            }
            HttpContext.Session.Set<List<ProjectModel>>("projects", projects);
            GlobalData.projects = HttpContext.Session.Get<List<ProjectModel>>("projects");
            GlobalData.cookies = HttpContext.Session.Get<string[,]>("cookies");

            return RedirectToAction("About");
        }

        // Частичное представление для вывода таблицы по проектам
        public IActionResult PaggingPartial()
        {
            return PartialView("_PaggingPartial");
        }

        // Вывод HtmlReportа
        [HttpPost]
        public IActionResult HtmlReport(string runId, string projectName)
        {
            string resultPath = "";
            try
            {
                string pcRunId = APIConnect.GetResultID(projectName, runId).Result;
                resultPath = APIConnect.GetHtmlReport(runId, pcRunId, projectName).Result;
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[HtmlReport] " + GlobalData.login + " " + ex.StackTrace);
                }
            }
            
            // Проверка наличия репорта по тест рану
            if (resultPath != "")
            {
                return Redirect("http://k10-ts2-wl1.r2.vtb24.ru:7181" + resultPath + "Report.html");
            }
            else
            {
                return View("NoResult");
            }
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
