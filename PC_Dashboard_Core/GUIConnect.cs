﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Text;
using System.IO;

namespace PC_Dashboard_Core
{
    public class GUIConnect
    {
        // Вход на сайт PC для сбора необходимых куков.
        /*public static async Task<string> LoginInPcGUI()
        {

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(GlobalData.serverPcProtocol +
                        GlobalData.serverPcDomen + "/loadtest/General/Login.aspx?logout=1");
                request.Method = "GET";
                request.Accept = "text/html, application/xhtml+xml, image/jxr";
                request.Headers.Add(HttpRequestHeader.Cookie, "Login=" + AlmProject.Convert64(GlobalData.login, GlobalData.password));
                request.Host = "k10-pc-server.r2.vtb24.ru";
                request.Connection = "Keep-Alive";

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[LoginInPcGUI_1] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(GlobalData.serverPcProtocol +
                        GlobalData.serverPcDomen + "/loadtest/General/Login.aspx?logout=1");
                request.Method = "POST";
                request.Headers.Add("X-Requested-With", "XMLHttpRequest");
                request.Headers.Add("X-MicrosoftAjax", "Delta=true");
                request.ContentType = "application/x-www-form-urlencoded; charset=utf-8";
                request.Headers.Add(HttpRequestHeader.Cookie, "Login=" + AlmProject.Convert64(GlobalData.login, GlobalData.password));
                request.Host = "k10-pc-server.r2.vtb24.ru";
                request.Connection = "Keep-Alive";

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[LoginInPcGUI_1] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return pcRunId;
        }*/

        // Получаем pcRunId из JSON ответа сервера
        /*public static async Task<string> ExtractPcRunId(string runid)
        {
            string PcRunId;

            try
            {
                byte[] ByteArr = Encoding.GetEncoding(1251).GetBytes("{\"runid\":" + runid + "}");

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(GlobalData.serverPcProtocol +
                        GlobalData.serverPcDomen + "/loadtest/Services/DashboardService.asmx/OpenOffline");
                request.Method = "POST";
                request.Accept = "application/json, text/plain";
                request.Headers.Add("X-XSRF-Header", /* добавить *//*"");
                request.ContentType = "application/json;charset=utf-8";
                request.Referer = GlobalData.serverPcProtocol + GlobalData.serverPcDomen + "/loadtest/HTML/StartPage/index.html";
                request.Headers.Add(HttpRequestHeader.Cookie, GlobalData.cookies/*УТОЧНИТЬ КУКУ*//*);
                request.ContentLength = ByteArr.Length;
                request.Host = "k10-pc-server.r2.vtb24.ru";
                request.Connection = "Keep-Alive";

                request.GetRequestStream().Write(ByteArr, 0, ByteArr.Length);

                HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync();
                StreamReader res = new StreamReader(response.GetResponseStream());
                PcRunId = res.ReadLine();
            }
            catch (Exception ex)
            {
                string logFile_path = @"C:\log\apdb.log";
                using (StreamWriter sw = new StreamWriter(logFile_path, true, System.Text.Encoding.Default))
                {
                    sw.WriteLine("[" + DateTime.Now + "] " + "[ExtractPcRunId] " + GlobalData.login + " " + ex.StackTrace);
                }
            }

            return PcRunId;
        }*/

    }
}
