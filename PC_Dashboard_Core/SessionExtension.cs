﻿using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace PC_Dashboard_Core
{
    // Сериализация/десериализация сложных объектов в/из Json строку для хранения в сессии
    public static class SessionExtension
    {
        public static void Set<T>(this ISession session, string key, T value)
        {
            session.SetString(key, JsonConvert.SerializeObject(value));
        }

        public static T Get<T>(this ISession session, string key)
        {
            var value = session.GetString(key);
            return value == null ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }
    }
}
