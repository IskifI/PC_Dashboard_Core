﻿using System.Collections.Generic;
using PC_Dashboard_Core.Models;

namespace PC_Dashboard_Core
{
    public class GlobalData
    {
        public static string login = "";
        public static string password = "";
        public static string serverProtocol = "http://";
        public static string serverDomen = "k10-alm.r2.vtb24.ru";
        public static string serverPort = "8080";
        public static string almDomenName = "LOADTEST";
        public static string serverPcProtocol = "http://";
        public static string serverPcDomen = "k10-pc-server.r2.vtb24.ru";
        public static string[,] cookies = new string[4, 2];
        public static List<ProjectModel> projects;
    }
}
